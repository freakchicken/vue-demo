import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        user: JSON.parse(sessionStorage.getItem(`user`))||{},
       
    },
    mutations: {
        setUser(state, payload) {
            state.user = payload;
            sessionStorage.setItem(`user`, JSON.stringify(payload))
        }
     
    },
    actions: {},
    getters: {

    }

})